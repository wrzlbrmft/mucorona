#!/usr/bin/env sh
while true; do
    (cd src && python main.py)
    echo Restarting in 3s...
    sleep 3
done
