from bs4 import BeautifulSoup
from dotenv import load_dotenv
import json
import logging
import pickle
from pyaml_env import parse_config
import re
import requests
import sys
from telegram import constants
from telegram.error import TelegramError
from telegram.ext import Updater
from telegram.utils import helpers
import yaml
from yaml.scanner import ScannerError

load_dotenv()

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

# load config from config.yml
try:
    logging.info('load config from config.yml')
    config = parse_config('config.yml')

    telegram_token = config['telegram']['token']
    telegram_chat_ids = config['telegram']['chat_ids']
    log_chat_id = config['telegram']['log_chat_id']

    url = config['url']
    timeout_sec = int(config['timeout_sec'])
    interval_sec = int(config['interval_sec'])
    cache_file = config['cache_file']
except (OSError, ScannerError, KeyError, ValueError) as e:
    logging.error(e)
    sys.exit(1)

# restore/init cache (last incidence)
try:
    logging.info('restore cache (last incidence)')
    with open(cache_file, 'rb') as f:
        last_incidence = pickle.load(f)
except OSError as e:
    logging.error(e)
    logging.info('init cache (last incidence)')
    last_incidence = {}

# init telegram bot
try:
    logging.info('init telegram bot')
    updater = Updater(token=telegram_token)
except TelegramError as e:
    logging.error(e)
    sys.exit(1)


def get_incidence(context):
    try:
        logging.info('get incidence - url=%r', url)
        response = requests.get(url, timeout=timeout_sec)
        soup = BeautifulSoup(response.content, 'html.parser')

        scripts = soup.find_all('script')
        script = [
            i
            for i in scripts
            if i.string and 'window.infographicData' in i.string
        ][0].string
        m = re.search(r'window.infographicData=(.*);', script)
        infographic_data = json.loads(m.group(1))
        entities = infographic_data['elements']['content']['content']['entities']

        charts = [
            entities[i]['props']['chartData']['data'][0]
            for i in entities.keys()
            if 'props' in entities[i] and 'chartData' in entities[i]['props']
        ]

        chart_incidence = [
            i
            for i in charts
            if i[0] and i[0][-1] and 'Inzidenz' in i[0][-1]
        ][0]

        incidence_date = chart_incidence[-1][0]
        incidence_value = str(chart_incidence[-1][-1])
        if '.' not in incidence_value and ',' not in incidence_value:
            incidence_value = incidence_value + '.0'
        incidence_value = incidence_value.replace(',', '.')
        s = incidence_value.split('.')
        incidence_value = float(''.join(s[:-1]) + '.' + s[-1])
        logging.info('got incidence - date=%r, value=%r', incidence_date, incidence_value)
        return {
            'date': incidence_date,
            'value': incidence_value
        }
    except (requests.RequestException, json.JSONDecodeError, NameError, TypeError, IndexError) as e:
        logging.error(e)
        try:
            context.bot.send_message(log_chat_id, text=str(e), parse_mode=constants.PARSEMODE_HTML,
                                     disable_web_page_preview=True)
        except TelegramError as e:
            logging.error(e)
        return None


def check_incidence(context):
    global last_incidence

    incidence = get_incidence(context)
    if incidence:
        if not last_incidence:
            # first incidence ever
            logging.info('first incidence - incidence=%r', incidence)
            text = ''
        elif incidence['date'] != last_incidence['date']:
            # new incidence on a new day
            logging.info('new incidence - incidence=%r', incidence)
            text = 'Neue '
        elif incidence['value'] != last_incidence['value']:
            # updated incidence on the same day
            logging.info('updated incidence - incidence=%r', incidence)
            text = 'Korrigierte '
        else:
            # same incidence, nothing to do
            logging.info('same incidence - incidence=%r -> nothing to do', incidence)
            return

        text = text + '[Inzidenz](%s) am %s: *%s*' % \
               (url,
                helpers.escape_markdown(incidence['date'], 2),
                helpers.escape_markdown(str(incidence['value']).replace('.', ','), 2))

        if last_incidence:
            # add delta for new/updated incidences
            delta = incidence['value'] - last_incidence['value']
            if delta > 0:
                emoji = '↗️'
            elif delta < 0:
                emoji = '↘️'
            else:
                emoji = '➡️'
            text = emoji + ' ' + text + helpers.escape_markdown((' (%+.1f)' % delta).replace('.', ','), 2)

        for chat_id in telegram_chat_ids:
            try:
                logging.info('send message - chat_id=%r', chat_id)
                context.bot.send_message(chat_id, text=text, parse_mode=constants.PARSEMODE_MARKDOWN_V2,
                                         disable_web_page_preview=True)
            except TelegramError as e:
                logging.error(e)

        last_incidence = incidence
        try:
            logging.info('update cache (last incidence)')
            with open(cache_file, 'wb') as f:
                pickle.dump(incidence, f)
            with open(f'{cache_file}.yml', 'wt') as f:
                yaml.dump(incidence, f)
        except OSError as e:
            logging.error(e)
            try:
                context.bot.send_message(log_chat_id, text=str(e), parse_mode=constants.PARSEMODE_HTML,
                                         disable_web_page_preview=True)
            except TelegramError as e:
                logging.error(e)


def main():
    updater.job_queue.run_repeating(check_incidence, interval_sec, first=1)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
